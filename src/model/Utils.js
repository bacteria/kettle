/**
 * Created by lenovo on 2017/3/16.
 */

export let Utils={
  isArray(obj){
    return Array.isArray(obj)
  },

  /**
   * 判断是否为空字符串，返回结果为true，视为非空字符串
   * */
  isEmptyStr(str){
    return str != null && str.length > 0;
  }
}


