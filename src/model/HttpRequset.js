import axios from "axios";

let server;
if(process.env.NODE_ENV == "development")
   server="http://localhost:8080/ydp";
else
   server=context;

export default {
  requestSourceList (){
     return axios.get(server +"/etl/dataSource/getDs.do")
  },
  requestSourceInstanceById(dsid){
    if (dsid) {
      return  axios.get(server +"/dbMeta/table/queryAllTableByDSID.do", {
        params: {dsid}
      })
    }
  },
  tableInputGetSql(joins,dsid){
    return axios.post(server +"/TableInput/buildSql.do",{joins,dsid})
  },
  tableInputGetSimpleSql(table,dsid){
    return axios.post(server +"/TableInput/buildSimpleSql.do",{table,dsid})
  },
  saveTransformation(trans){
    let inteface = "";
    if(trans.id ===""){
      inteface = "/trans/save.do";
    }else{
      inteface = "/trans/update.do"
    }
    return axios.post(server +inteface,trans)
  },
  requestSavingStepsFromServer(id){
    return axios.get(server +"/trans/open.do",{
      params:{id}
    })
  },
  nameExistCheck(name,id){
    return axios.get(server +"/trans/nameExistCheck.do",{
      params:{name,id}
    })
  },
  testSql(Sql,dsId){
    let ajax = axios.create({timeout:10000});
    return ajax.post(server +"/TableInput/getSQLMeta.do",{
      Sql,dsId
    })
  },
  //webservice Analysis
  webServiceAnalysis({url,user="",password=""}){
    return axios.get(server+"/WebService/loadWdsl.do",{
      params:{url,user,password}
    })
  }
};
