/**
 * 后台数据源数据结构
 * */
export function DataSource() {
  return {
    sourceList: [{name: "", id: ""}],
    /**
     * tables:[{
      "name": "product",
      "cnname": "产品信息表",
      "columnList": [
      {"name": "pname", "cnname": "产品名称"},
      {"name": "pid", "cnname": "产品ID"},
      {"name": "pprice","cnname": "产品价格" }
      ]}]
     *
     * */
    tables: []
  }
}
/**
 * step列表数据
 * */
export function treeView() {
  return [
    {
      title: "输入",
      items: [
        {name: "表输入", type: "TableInput"},
        {name: "生成记录", type: "RowGenerator"},
        {name: "获取系统信息", type: "SystemInfo"},
        {name: "文件加载", type: "LoadFileInput"},
        {name: "xml输入", type: "getXMLData"},
        {name: "Json输入", type: "JsonInput"},
      ],
      collapsed: false
    },
    {
      title: "输出",
      items: [
        {name: "表输出", type: "TableOutput"},
        {name: "插入/更新", type: "InsertUpdate"},
       // {name: "执行存储过程", type: "DBProc"},
        {name: "文本文件输出", type: "TextFileOutput"},
      ],
      collapsed: false
    },
    {
      title: "流程",
      items: [
        {name: "空操作", type: "Dummy"},
        {name: "过滤", type: "FilterRows"},
      ],
      collapsed: false
    },
    {
      title: "查询",
      items: [
        {name: "Http客户端", type: "HTTP"},
        {name: "Http post", type: "HTTPPOST"},
        {name: "Web服务", type: "WebServiceLookup"},
      ],
      collapsed: true
    },
    {
      title: "转换",
      items: [
        {name: "计算器", type: "Calculator"},
        {name: "去重", type: "Unique"},
        {name: "字符串替换", type: "ReplaceString"},
        {name: "排序", type: "SortRows"},
        {name: "行转列", type: "Normaliser"}
      ],
      collapsed: true
    },
    {
      title: "作业",
      items: [
        {name: "获取变量", type: "GetVariable"},
        {name: "设置变量", type: "SetVariable"}
      ],
      collapsed: true
    },
    {
      title: "脚本",
      items: [
        {name: "SQL脚本", type: "ExecSQL"}
      ],
      collapsed: true
    },
    {
      title: "检验",
      items: [
        {name: "数据检验", type: "Validator"}
      ],
      collapsed: true
    }

  ]
}
/**
 * step主数据结构
 * */
export const Steps = {
  Dummy(obj){
    let step = {"type": "Dummy", "typeDesc": "空操作"};
    return this.__common__(step, obj);
  },
  TableInput(obj){
    let step = {
      "type": "TableInput",
      "typeDesc": "表输入",
      "connection": null,
      "sql": null,
      "limit": 0,
      "execute_each_row": null,
      "variables_active": null,
      "lazy_conversion_active": null,
      "ext": {
        "tables": [],
        "joins": [{
          "parent": "",
          "client": "",
          "joinType": "INNER JOIN",
          "conditions": [{"field": "", "cndType": "=", "value": "", "index": 0}],
          "logic": "#0"
        }],
        "jointype": ["INNER JOIN", "LEFT JOIN", "RIGHT JOIN"],
        "conditiontype": ["=", "<>", "<", ">", "<=", ">="]
      }
    };
    return this.__common__(step, obj);
  },
  TableOutput(obj){
    let step = {
      "type": "TableOutput",
      "typeDesc": "表输出",
      "connection": "",
      "schema": "",
      "table": "",
      "commit": 1000,
      "truncate": "N",
      "ignore_errors": "N",
      "use_batch": "Y",
      "specify_fields": "Y",
      "partitioning_enabled": "N",
      "partitioning_field": "",
      "partitioning_daily": "N",
      "partitioning_monthly": "Y",
      "tablename_in_field": "N",
      "tablename_field": "",
      "tablename_in_table": "Y",
      "return_keys": "N",
      "return_field": "",
    };
    return this.__common__(step, obj);
  },
  FilterRows(obj){
    let step = {
      "type": "FilterRows",
      "typeDesc": "过滤",
      "send_true_to": "",
      "send_false_to": "",
      "negated": "N",
      "conditions": []
    };
    return this.__common__(step, obj);

  },
  Unique(obj){
    let step = {
      "type": "Unique",
      "typeDesc": "去重",
      "count_rows": "N",
      "count_field": "",
      "reject_duplicate_row": "N",
      "error_description": ""
    };
    return this.__common__(step, obj)
  },
  ReplaceString(obj){
    let step = {
      "type": "ReplaceString",
      "typeDesc": "字符串替换",
    };
    return this.__common__(step, obj)
  },
  SortRows(obj){
    let step = {
      "type": "SortRows",
      "typeDesc": "排序",
    };
    return this.__common__(step, obj)
  },
  GetVariable(obj){
    let step = {
      "type": "GetVariable",
      "typeDesc": "获取变量",
    };
    return this.__common__(step, obj)
  },
  Normaliser(obj){
    let step = {
      "type": "Normaliser",
      "typeDesc": "列转行",
      "typefield": "",
    };
    return this.__common__(step, obj)
  },
  SetVariable(obj){
    let step = {
      "type": "SetVariable",
      "typeDesc": "设置变量",
      "use_formatting": "Y"
    };
    return this.__common__(step, obj)
  },
  ExecSQL(obj){
    let step = {
      type: "ExecSQL",
      typeDesc: "执行SQL脚本",
      connection: "",
      sql: "",
      insert_field: "",
      update_field: "",
      delete_field: "",
      read_field: ""
    };
    return this.__common__(step, obj)
  },
  Calculator(obj){
    let step = {
      type: "Calculator",
      typeDesc: "计算器",
    };
    return this.__common__(step, obj)
  },
  RowGenerator(obj){
    let step = {
      type: "RowGenerator",
      typeDesc: "生成记录",
      limit: 10,
    };
    return this.__common__(step, obj)
  },
  SystemInfo(obj){
    let step = {
      type: "SystemInfo",
      typeDesc: "获取系统信息"
    };
    return this.__common__(step, obj)
  },
  InsertUpdate(obj){
    let step = {
      type: "InsertUpdate",
      typeDesc: "插入/更新",
      connection: "",
      update_bypassed: "",
      table: "",
      commit: 100,
      queryList: [],
      updateList: []
    };
    return this.__common__(step, obj)
  },
  Validator(obj){
    let step = {
      type: "Validator",
      typeDesc: "数据检验",
      send_false_to: "",
      validate_all: "N",
      concat_errors: "N",
      concat_separator: "",
    };
    return this.__common__(step, obj)
  },
  WebServiceLookup(obj){
    let step = {
      type: "WebServiceLookup",
      typeDesc: "Web服务",
      wsURL: "",
      wsOperation: "",
      wsOperationNamespace: "",
      wsOperationRequest: "",
      proxyHost: "",
      proxyPort: "",
      httpLogin: "",
      httpPassword: "",
      callStep: 1,
      passingInputData: "Y",
      compatible: "N",
      repeating_element: "",
      reply_as_string: "N",
      fieldsInList: [],
      fieldsOutList: []
    };
    return this.__common__(step, obj)
  },
  HTTP(obj){
    let step = {
      type: "HTTP", typeDesc: "Http Client", url: "", urlInField: "N",
      urlField: "", encoding: "UTF-8", httpLogin: "", httpPassword: "", proxyHost: "", proxyPort: "",
      socketTimeout: 10000, connectionTimeout: 10000, closeIdleConnectionsTime: -1,
      paramList: [], headerList: [],
      result: {name: "result", code: "", response_time: "", response_header: ""},
    };
    return this.__common__(step, obj)
  },
  HTTPPOST(obj){
    let step = {
      type: "HTTPPOST", typeDesc: "Http Post", url: "", urlInField: "N",
      urlField: "", encoding: "UTF-8", httpLogin: "", httpPassword: "", proxyHost: "", proxyPort: "",
      socketTimeout: 10000, connectionTimeout: 10000, closeIdleConnectionsTime: -1,
      argList: [], queryList: [], postafile: "N", requestEntity: "",
      result: {name: "result", code: "", response_time: "", response_header: ""},
    };
    return this.__common__(step, obj)
  },
  LoadFileInput(obj){
    let step = {
      type: "LoadFileInput", typeDesc: "文件加载",
      fileName: "", filemask: "", exclude_filemask: "", file_required: "N", include_subfolders: "N",
      isInFields: "N", dynamicFilenameField: ""
    };
    return this.__common__(step, obj)
  },
  JsonInput(obj){
    let step = {type: "JsonInput", typeDesc: "Json 输入", isAFile: "N", readurl: "N", valueField: ""};
    return this.__common__(step, obj)
  },
  getXMLData(obj){
    let step = {type: "getXMLData", typeDesc: "Xml 输入", readurl: "N", isAFile: "N", xmlField: "", loopxpath: ""};
    return this.__common__(step, obj);
  },
  TextFileOutput(obj){
    let step = {
      type: "TextFileOutput", typeDesc: "文本文件输出",
      separator: ";",
      enclosure: '"',
      enclosure_forced: "N",
      header: "N",
      footer: "N",
      format: "DOS",
      fileNameInField: "N",
      fileNameField: "",
      file: {name: "", extention: "TXT", add_date: "N", add_time: "N"}
    };
    return this.__common__(step, obj);
  },
  DBProc(obj){
    let step = {
      type: "DBProc", typeDesc: "执行存储过程",
      result_name: "", result_type: "String",auto_commit:"Y",procedure:"",connection:""
    };
    return this.__common__(step, obj);
  },
  __common__(step, {_num = 0, name = "", x = 0, y = 0, id = new Date().getTime()} = {}){
    return Object.assign({}, step, {
        name, x, y, id, "availableFields": [], _num,
        "outputFields": [], "fields": []
      }
    )
  }
};

/**
 * step子表行数据结构
 * */
export const Fields = {
  ReplaceString({
                  edit = false,
                  in_stream_name = "",
                  out_stream_name = "",
                  use_regex = "no",
                  replace_string = "",
                  replace_by_string = "",
                  set_empty_string = "N",
                  replace_field_by_string = "",
                  whole_word = "no",
                  case_sensitive = "no",
                } = {}){
    return {
      edit, in_stream_name, out_stream_name, use_regex,
      replace_string, replace_by_string, set_empty_string, replace_field_by_string,
      whole_word, case_sensitive
    };
  },
  SortRows({edit = false, name = "", case_sensitive = "N", ascending = "N"}){
    return {edit, name, case_sensitive, ascending};
  },
  FilterRows({
               edit = false, negated = "N", leftvalue = "", func = "", rightvalue = "", operator = "OR",
               value: {name = "constant", type = "String", text = "", length = "-1", precision = "-1", isnull = "N", mask = ""} = {}
             } = {}){
    return {
      edit,
      negated,
      leftvalue,
      "function": func,
      rightvalue,
      operator,
      value: {name, mask, type, text, length, precision, isnull}
    }
  },
  Unique({edit = false, name = "", case_insensitive = "Y"} = {}){
    return {edit, name, case_insensitive}
  },
  GetVariable({edit = false, name = "", variable = "", type = "String"}){
    return {edit, name, variable, type}
  },
  Normaliser({edit = false, name = "", value = "", norm = ""} = {}){
    return {edit, name, value, norm}
  },
  SetVariable({edit = false, field_name = "", variable_name = "", variable_type = "JVM", default_value = ""} = {}){
    return {edit, field_name, variable_name, variable_type, default_value}
  },
  ExecSQL({edit = false, name = ""} = {}){
    return {edit, name}
  },
  Calculator({edit = false, field_name = "", calc_type = "", field_a = "", field_b = "", field_c = "", value_type = ""} = {}){
    return {edit, field_a, field_b, field_c, field_name, value_type, calc_type}
  },
  RowGenerator({edit = false, name = "", type = "String", set_empty_string = "N", nullif = ""} = {}){
    return {edit, name, type, set_empty_string, nullif}
  },
  SystemInfo({edit = false, name = "", type = ""} = {}){
    return {edit, name, type};
  },
  InsertUpdate({edit = false, name = "", type = "query", rename = "", update = "Y", name2 = "", condition = "=", field = ""} = {}){
    if (type == "update") {
      return {edit, name, rename, update}
    } else
      return {edit, name, name2, condition, field};
  },
  Validator(obj = {}){
    let {validation_name = ""} = obj;
    return this.__common__({
      name: "",
      validation_name,
      max_length: "",
      min_length: "",
      null_allowed: "Y",
      conversion_mask: "",
      decimal_symbol: "",
      grouping_symbol: "",
      only_null_allowed: "N",
      only_numeric_allowed: "N",
      data_type: "None",
      data_type_verified: "N",
      max_value: "",
      min_value: "",
      start_string: "",
      end_string: "",
      start_string_not_allowed: "",
      end_string_not_allowed: "",
      regular_expression: "",
      regular_expression_not_allowed: "",
      error_code: "",
      error_description: "",
      is_sourcing_values: "N",
      sourcing_step: "",
      sourcing_field: "",
      allowed_value: "",
    }, obj)
  },
  HTTP({edit = false, name = "", parameter = ""} = {}){
    return {edit, name, parameter}
  },
  HTTPPOST({edit = false, name = "", parameter = "", header = "N", type = "query"} = {}){
    if (type == "arg") {
      return {edit, name, parameter, header}
    } else {
      return {edit, name, parameter}
    }
  },
  WebServiceLookup({edit = false, name = "", wsName = "", xsdType = ""} = {}){
    return {edit, name, wsName, xsdType}
  },
  LoadFileInput({edit = false, name = "", element_type = "", type = "", trim_type = ""} = {}){
    return {edit, name, element_type, type, trim_type}
  },
  JsonInput({edit = false, name = "", path = "", type = "", length = "", precision = "", trim_type = ""} = {}){
    return {edit, name, path, type, length, precision, trim_type}
  },
  getXMLData({edit = false, name = "", xpath = "", element_type = "node", result_type = "valueof", type = "", trim_type = "none"} = {}){
    return {edit, name, xpath, element_type, result_type, type, trim_type}
  },
  TextFileOutput({edit = false, name = "", type = "", format = "", trim_type = "none"} = {}){
    return {edit, name, type, format, trim_type}
  },
  DBProc({edit = false, name = "", direction = "OUT", type = "String"} = {}){
    return {edit, name, type, direction}
  },
  __common__(step, {_num = 0, edit = false} = {}){
    return Object.assign({_num, edit}, step,)
  }
};

/**
 * 下拉选择数据集合
 * */
export const Options = {
  func: [
    {label: ">"},
    {label: ">="},
    {label: "<="},
    {label: "="},
    {label: "<>"},
    {label: "<",},
    {label: "REGEXP"},
    {label: "IS NULL"},
    {label: "IS NOT NULL"},
    {label: "CONTAINS"},
    {label: "STARTS WITH"},
    {label: "ENDS WITH"},
    {label: "LIKE"},
    {label: "TRUE"},
    {label: "IN LIST"}
  ],
  inputTypes: [
    "String", "Timestamp", "Integer", "Number", "BigNumber", "Date", "Binary", "Boolean", "Internet Address"
  ],
  format: {
    Number: ["#,##0.###", "0.00", "#", "#.#", "###,###,###.# "],
    Date: ["yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm:ss.SSS", "yyyy/MM/dd"]
  },
  operators: ["OR", "OR NOT", "XOR", "AND", "AND NOT"],
  variableType: [{value: "JVM", name: "JVM"}, {value: "PARENT_JOB", name: "PARENT_JOB"}, {
    value: "GP_JOB",
    name: "Grand_PARENTJOB"
  }, {value: "ROOT_JOB", name: "ROOT_JOB"}],
  calcType: [{value: "ADD", name: "A + B"}, {value: "SUBTRACT", name: "A - B"}, {
    value: "MULTIPLY",
    name: "A * B"
  }, {value: "DIVIDE", name: "A / B"}],
  sysDataType: [{value: "system date (variable)", name: "\u7cfb\u7edf\u65e5\u671f (\u53ef\u53d8)"},
    {value: "system date (fixed)", name: "\u7cfb\u7edf\u65e5\u671f (\u56fa\u5b9a)"},
    {value: "yesterday start", name: "\u6628\u5929 00:00:00"},
    {value: "yesterday end", name: "\u6628\u5929 23:59:59"},
    {value: "today start", name: "\u4eca\u5929 00:00:00"},
    {value: "today end", name: "\u4eca\u5929 23:59:59"},
    {value: "tomorrow start", name: "\u660e\u5929 00:00:00"},
    {value: "tomorrow end", name: "\u660e\u5929 23:59:59"},
    {value: "last month start", name: "\u4e0a\u6708\u7b2c\u4e00\u5929\u7684 00:00:00"},
    {value: "last month end", name: "\u4e0a\u6708\u6700\u540e\u4e00\u5929\u7684 23:59:59"},
    {value: "this month start", name: "\u672c\u6708\u7b2c\u4e00\u5929\u7684 00:00:00"},
    {value: "this month end", name: "\u672c\u6708\u6700\u540e\u4e00\u5929\u7684  23:59:59"},
    {value: "next month start", name: "\u4e0b\u4e2a\u6708\u7b2c\u4e00\u5929\u7684 00:00:00"},
    {value: "next month end", name: "\u4e0b\u4e2a\u6708\u6700\u540e\u4e00\u5929\u7684 23:59:59"},
    {value: "last week start", name: "LastWeekStart"},
    {value: "last week end", name: "LastWeekEnd"},
    {value: "last week open end", name: "LastWeekOpenEnd"},
    {value: "this week start", name: "ThisWeekStart"},
    {value: "this week end", name: "ThisWeekEnd"},
    {value: "this week open end", name: "ThisWeekOpenEnd"},
    {value: "next week start", name: "NextWeekStart"},
    {value: "next week end", name: "NextWeekEnd"},
    {value: "next week open end", name: "NextWeekOpenEnd"},
    {value: "prev quarter start", name: "PrevQuarterStart"},
    {value: "prev quarter end", name: "PrevQuarterEnd"},
    {value: "this quarter start", name: "ThisQuarterStart"},
    {value: "this quarter end", name: "ThisQuarterEnd"},
    {value: "next quarter start", name: "NextQuarterStart"},
    {value: "next quarter end", name: "NextQuarterEnd"},
    {value: "prev year start", name: "PrevYearStart"},
    {value: "prev year end", name: "PrevYearEnd"},
    {value: "this year start", name: "ThisYearStart"},
    {value: "this year end", name: "ThisYearEnd"},
    {value: "next year start", name: "NextYearStart"},
    {value: "next year end", name: "NextYearEnd"}],
  whereCondition: ["=", "<>", "<", "<=", ">", ">=", "LIKE", "BETWEEN", "IS NULL", "IS NOT NULL"],
  trimType: [{name: "不去除", value: "none"}, {name: "去除左空格", value: "left"}, {
    name: "去除右空格",
    value: "right"
  }, {name: "去除左右空格", value: "both"}],
  directions:[{name:"输出",value:"OUT"},{name:"输入",value:"IN"},{name:"输入/输出",value:"INOUT"}]
}
