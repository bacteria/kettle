/**
 * Created by lenovo on 2017/2/24.
 */
import Vuex from 'vuex'
import Vue from 'vue'
import step from "./StepStore";
Vue.use(Vuex);

export default new Vuex.Store({
  modules:{step:step}
});
