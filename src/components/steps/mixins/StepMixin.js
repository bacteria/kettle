/**
 * Created by lenovo on 2017/3/1.
 */
import store from "../../../store/store";
import cloneDeep from "lodash/cloneDeep";
import {Fields} from "../../../model/DataModel";

export default {
  /**
   * step详情页面加载的时候从vuex获取step数据
   * */
  mounted(){
    this.step = cloneDeep(this.$store.getters.nodeInfo);
    this.$emit("ModalOpened",this.customParams)
  },
  store,
  /**
   * step详情页面销毁的时候将数据提交到vuex
   * */
  destroyed(){
    if (this.$store.getters.isSave) {
      if (this.updateFields) {
        this.updateFields();
      }
      this.$store.commit("addNode", this.step);
    }
  },
  computed: {
    //计算出下一步有哪些子节点
    nextSteps(){
      let nextStepIds = [], stepId = this.step.id, stepInfo = this.$store.state.step.steps;
      for (let key in this.$store.state.step.connections)
        if (key.endsWith(stepId))
          nextStepIds.push(key.split("||")[0]);
      return nextStepIds.map(el => ({name: stepInfo[el].name, id: el}))
    }
  },
  data(){
    return {
      rules: {
        name(rule, value, callback, source, options) {
          let errors = [];
          if (/.*([\\*#.^!$&+]).*/.test(value)) {
            errors.push(new Error("名称中不能包含下列特殊字符 \\、*、#、.、^、!、$、&、+"))
          }
          callback(errors);
        }
      }
    }
  }
  ,
  methods: {
    /**
     * 通用提示
     * @param msg
     */
  warning(msg){
    this.$message({
      message: msg,
      type: 'warning'
    });
  },
  notify(msg){
    this.$notify({
      title: '警告',
      message: msg,
      type: 'warning'
    });
  },
  success(msg){
    this.$message({
      message: msg,
      type: 'success'
    });
  },
    confirm(msg,fun){
    this.$msgbox({
      title:'提示',
      message:msg,
      showCancelButton:true,
      confirmButtonText: '确定',
      cancelButtonText: '取消'
    }).then((action)=>{
      if(action==='confirm') {
        fun();
      }
    });
},
    /**
     * 通用删除数据
     * **/
    handleDelete(index, row, targetArray){
      if (targetArray && Array.isArray(targetArray)) {
        targetArray.splice(index, 1);
      }
      else {
        this.step.fields.splice(index, 1);
      }
    },
    /**
     * 通用添加数据
     * **/
    _addRecord(){
      let name = this.step.type;
      if (name) {
        this.step.fields.push(Fields[name]({edit: false}));
      }
    },
    /**
     * 转换表格编辑状态
     * **/
    reverseEditState(index, row){
      row.edit = !row.edit;
    },
    beforeSave(saveFun){
      let form = this.$refs['form'];
      if (form) form.validate((valid) =>{if (valid) saveFun.call()});
    }
  }

}
