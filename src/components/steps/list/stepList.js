/**
 * Created by lenovo on 2017/2/22.
 */
import TableInput from "../TableInput.vue";
import Vue from 'vue';
import Step from "../../StepDraw.vue";
import TableOutput from "../TableOutput.vue";
import ReplaceString from "../ReplaceString.vue";
import Dummy from "../Dummy.vue";
import Unique from "../Unique.vue";
import SortRows from "../SortRows.vue";
import FilterRows from "../FilterRows.vue";
import GetVariable from "../GetVariable.vue";
import Normaliser from "../Normaliser.vue";
import SetVariable from "../SetVariable.vue";
import ExecSQL from "../ExecSQL.vue";
import Calculator from "../Calculator.vue";
import RowGenerator from "../RowGenerator.vue";
import HelpInfo from "../../HelpInfo.vue";
import SystemInfo from "../SystemInfo.vue";
import InsertUpdate from "../InsertUpdate.vue";
import Validator from "../Validator.vue";
import HTTP from "../HTTP.vue";
import WebServiceLookup from "../WebServiceLookup.vue";
import LoadFileInput from "../LoadFileInput.vue";
import JsonInput from "../JsonInput.vue";
import HTTPPOST from "../HTTPPOST.vue";
import getXMLData from "../GetXMLData.vue";
import TextFileOutput from "../TextFileOutput.vue";
import DBProc from "../DBProc.vue";

const stepEditors = {
  TableInput,TableOutput,ReplaceString,Dummy,Unique,SortRows,SystemInfo,InsertUpdate,Validator,getXMLData,DBProc,
  FilterRows,GetVariable,Normaliser,SetVariable,ExecSQL,Calculator,RowGenerator,HTTP,LoadFileInput,JsonInput,WebServiceLookup,HTTPPOST,TextFileOutput
};

for(let editor in stepEditors){
  Vue.component(editor,stepEditors[editor])
}

Vue.component("stepNode",Step);
Vue.component("helpInfo",HelpInfo);
export default stepEditors;
